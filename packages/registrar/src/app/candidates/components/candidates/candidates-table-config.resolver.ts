import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class CandidatesTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
      return import(`./candidates-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./candidates-table.config.list.json`);
        });
    }
}

export class CandidatesTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./candidates-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./candidates-table.search.list.json`);
            });
    }
}
