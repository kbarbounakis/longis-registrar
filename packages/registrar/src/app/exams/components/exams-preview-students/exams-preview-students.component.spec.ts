import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewStudentsComponent } from './exams-preview-students.component';

describe('ExamsPreviewStudentsComponent', () => {
  let component: ExamsPreviewStudentsComponent;
  let fixture: ComponentFixture<ExamsPreviewStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
