import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-currentgrades',
  templateUrl: './students-overview-currentgrades.component.html',
  styleUrls: ['./students-overview-currentgrades.component.scss']
})
export class StudentsOverviewCurrentgradesComponent implements OnInit, OnDestroy  {

  public recentGrades: any;
  public lastYearPeriod: any;
 studentId: number;
  private subscription: Subscription;

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
    this.lastYearPeriod = await this._context.model('students/' + this.studentId + '/courses')
      .select( 'gradePeriod', 'gradeYear')
      .where('gradeYear').notEqual(null)
      .and('gradePeriod').notEqual(null)
      .groupBy('gradePeriod', 'gradeYear')
      .orderByDescending('gradeYear')
      .thenByDescending('gradePeriod')
      .take(1)
      .getItem();

    if (this.lastYearPeriod) {
      this.recentGrades = await this._context.model('students/' + this.studentId + '/courses')
        .where('gradeYear/id').equal(this.lastYearPeriod.gradeYear.id)
        .and('gradePeriod/id').equal(this.lastYearPeriod.gradePeriod.id)
        .expand('course($expand=instructor)')
        .take(3)
        .getItems();
    } else {
      this.recentGrades = null;
    }
   });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
