import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ClassesFormComponent} from './components/classes-dashboard/classes-form/classes-form.component';
import {AppEventService, SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
// tslint:disable-next-line:max-line-length
import { ClassStudentsSearchResolver, ClassDefaultStudentsConfigurationResolver, ClassStudentsConfigurationResolver } from './components/classes-dashboard/classes-students/class-students-config.resolver';
// tslint:disable-next-line:max-line-length
import { ClassInstructorsSearchResolver, ClassInstructorsConfigurationResolver, ClassDefaultInstructorsConfigurationResolver } from './components/classes-dashboard/classes-instructors/class-instructor-config.resolver';
import { InstructorsSharedModule } from '../instructors/instructors.shared';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    ClassesFormComponent
  ],
  providers: [
    ClassStudentsSearchResolver,
    ClassStudentsConfigurationResolver,
    ClassDefaultStudentsConfigurationResolver,
    ClassInstructorsSearchResolver,
    ClassInstructorsConfigurationResolver,
    ClassDefaultInstructorsConfigurationResolver
  ],
  exports: [
    ClassesFormComponent
  ]
})
export class ClassesSharedModule {

  constructor(private _translateService: TranslateService, private appEvent: AppEventService) {
    const sources = environment.languages.map((language: string) => {
      return import(`./i18n/classes.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
        return Promise.resolve();
      })
    });
    Promise.all(sources).then(() => {
      // emit event
      this.appEvent.add.next({
        service: this._translateService,
        type: this._translateService.setTranslation
      });
    }).catch((err) => {
      console.error('An error occurred while loading shared module');
      console.error(err);
    });
  }

}
