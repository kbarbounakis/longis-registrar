import {async, inject, TestBed} from '@angular/core/testing';
import { ApiTestingModule, TestingConfigurationService } from '@universis/common/testing';
import { RuleService } from './rule.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AngularDataContext, AngularDataService, MostModule} from '@themost/angular';
import {APP_BASE_HREF} from '@angular/common';
import {ConfigurationService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {DEFAULT_RULES_CONFIG, RULES_CONFIG} from './rule.configuration';

describe('RuleService', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        ApiTestingModule.forRoot()
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ],
    }).compileComponents();
  }));

  it('should create an instance', inject([AngularDataContext],
    (context: AngularDataContext) => {
      const service = new RuleService(context, DEFAULT_RULES_CONFIG);
      expect(service).toBeTruthy();
    }));

  it('should get rules', inject([AngularDataContext],
    async (context: AngularDataContext) => {
      const service = new RuleService(context, DEFAULT_RULES_CONFIG);
      spyOn(ClientDataQueryable.prototype, 'getItems').and.callFake(
        function() {
          return Promise.resolve([]);
        }
      );
      const results = service.get( 'CourseClasses', '1000', 'RegistrationRules' );
      expect(results).toBeTruthy();
    }));

});
