import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-enrollment-event-requests',
  templateUrl: './event-overview-requests.component.html'
})
export class EnrollmentEventRequestsComponent implements OnInit {

  @Input() public enrollmentEvent: any;


  constructor(private _context: AngularDataContext) { }
  public requestSummary;
  public total;
  async ngOnInit() {
    this.requestSummary = await this._context.model('StudyProgramRegisterActions')
      .where('studyProgramEnrollmentEvent').equal(this.enrollmentEvent.id)
      .groupBy('actionStatus')
      .select('count(id) as total, actionStatus')
      .getItems();
    this.total = this.requestSummary.reduce((sum, info) => sum + info.total, 0);
  }
}
