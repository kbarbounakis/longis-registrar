import { HomeComponent } from './home.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SectionsComponent } from './../sections/sections.component';
import { TranslateModule } from '@ngx-translate/core';
import { APP_BASE_HREF, CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StatisticsRoutingModule } from './../../statistics.routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ReportsSharedModule } from '../../../reports-shared/reports-shared.module';
import { StatisticsService } from './../../services/statistics-service/statistics.service';
import { SettingsModule } from './../../../settings/settings.module';
import { MostModule } from '@themost/angular';
import { SettingsService } from './../../../settings-shared/services/settings.service';
import { AuthModule, ConfigurationService, ErrorService, LoadingService, SharedModule } from '@universis/common';
import { TestingConfigurationService } from '@universis/common/testing';
import { BsModalService, ModalModule } from 'ngx-bootstrap';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './../../statistics.routing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockStatisticsService;

  beforeEach(() => {
    mockStatisticsService = jasmine.createSpyObj(['getReportTemplatesOfCategory', 'getStatisticsReportCategories']);
    mockStatisticsService.getStatisticsReportCategories.and.returnValue([]);
    mockStatisticsService.getReportTemplatesOfCategory.and.returnValue([]);
    
    const testingModule = TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AuthModule,
        TranslateModule.forRoot(),
        StatisticsRoutingModule,
        FormsModule,
        SharedModule.forRoot(),
        ModalModule.forRoot(),
        SettingsModule,
        RouterTestingModule.withRoutes(routes),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        NgArrayPipesModule,
        ReportsSharedModule.forRoot()
      ],
      declarations: [HomeComponent, SectionsComponent],
      providers: [
        SettingsService,
        LoadingService,
        ErrorService,
        BsModalService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        },
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: StatisticsService,
          useValue: mockStatisticsService
        }
      ]
    }).compileComponents();

    return testingModule;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should print the categories as tabs', () => {
    const originalList = fixture.debugElement.queryAll(By.css('.nav .nav-item'));
    component.availableRoutes = [{
      route: 'mockRoute1',
      label: 'mockRoute1'
    }, {
      route: 'mockRoute2',
      label: 'mockRoute2'
    }];

    fixture.detectChanges();
    const list = fixture.debugElement.queryAll(By.css('.nav .nav-item'));
    expect(originalList.length).toBe(2);
    expect(list.length).toBe(4);
    expect(list[1].nativeElement.textContent.trim()).toBe('mockRoute1');
    expect(list[2].nativeElement.textContent.trim()).toBe('mockRoute2');
  });

  it('should not show empty set indicator when there are categories', () => {
    component.availableRoutes = [{
      route: 'mockRoute1',
      label: 'mockRoute1'
    }];
    fixture.detectChanges();
    const emptyIndicator = fixture.debugElement.queryAll(By.css('.icon-circle.bg-gray-300.border-gray-100'));
    expect(emptyIndicator.length).toBe(0);
  });

  it('should show empty set indicator when there is no categories', () => {
    component.availableRoutes = [];
    fixture.detectChanges();
    const emptyIndicator = fixture.debugElement.queryAll(By.css('.icon-circle.bg-gray-300.border-gray-100'));
    expect(emptyIndicator.length).toBe(1);
  });
});
