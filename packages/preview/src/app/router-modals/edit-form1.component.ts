import {Component, Input, ViewChild, OnInit, OnDestroy, EventEmitter, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { RouterModalOkCancel } from '@universis/common/routing';
import {FormioComponent} from 'angular-formio';

import * as EDIT_FORM from './edit-form1.component.json';

@Component({
    selector: 'app-edit-form1',
    template: `<formio [form]="formData" (change)="onChange($event)" [refresh]="refreshForm"  #form></formio>`,
})
export class EditForm1Component extends RouterModalOkCancel implements OnInit, OnDestroy {

    // formData holds form.io definition that is going to be loaded from json
    @Input('formData') formData: any;

    @ViewChild('form') form: FormioComponent;

    @Output() refreshForm = new EventEmitter<any>();

    constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
        super(router, activatedRoute);
    }

    async ngOnInit() {
        // find submit button
        const findButton =  (<any>EDIT_FORM).components.find( component => {
            return component.type === 'button' && component.label === 'Submit';
        });
        // hide button
        if (findButton) {
            findButton.hidden = true;
        }
        // set form data
        this.formData = EDIT_FORM;
        // set modal title as it is defined in form
        this.modalTitle = (<any>EDIT_FORM).title;
        // do refresh
        this.refreshForm.emit(this.formData);
    }
    ngOnDestroy() {
        //
    }

    onChange(event: any) {
        // handle changes (check if event has isValid property)
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
            // enable or disable button based on form status
            this.okButtonDisabled = !event.isValid;
        }
    }

    ok() {
        // do something and close
        return this.close();
    }

    cancel() {
        // cancel and close form
        return this.close();
    }

}
