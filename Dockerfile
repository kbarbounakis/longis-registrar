FROM node:14-buster AS builder

# create app directory
RUN mkdir -p /usr/src/app /usr/src/app/packages/registrar /usr/src/app/packages/forms /usr/src/app/packages/common /usr/src/app/theme
WORKDIR /usr/src/app
# copies package.json and package lock to
# take advantage of layering in image building
COPY package.json package-lock.json /usr/src/app/
COPY packages/registrar/package.json packages/registrar/package-lock.json /usr/src/app/packages/registrar/
COPY packages/common/package.json packages/common/package-lock.json /usr/src/app/packages/common/
COPY packages/forms/package.json packages/forms/package-lock.json /usr/src/app/packages/forms/
COPY packages/theme/package.json packages/theme/package-lock.json /usr/src/app/packages/theme/
RUN npm ci && cd packages/common && npm ci && cd ../forms && npm ci && cd ../theme && npm ci && cd ../

COPY . /usr/src/app
RUN npm run build


FROM nginx:stable-alpine AS production
COPY --from=builder /usr/src/app/dist/registrar /usr/share/nginx/html

EXPOSE 80
