import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableConfiguration } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-advanced-list-wrapper',
  templateUrl: './advanced-list-wrapper.component.html',
  styleUrls: ['./advanced-list-wrapper.component.scss']
})
export class AdvancedListWrapperComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  constructor(private readonly _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
            this.tableConfiguration = AdvancedTableConfiguration.cast(data.tableConfiguration);
        }
    });
  }

  ngOnDestroy(): void {
      if (this.dataSubscription) {
        this.dataSubscription.unsubscribe();
      }
  }

}
