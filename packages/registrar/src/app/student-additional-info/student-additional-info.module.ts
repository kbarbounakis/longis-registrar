import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { AdvancedFormsModule } from '@universis/forms'
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterModalModule } from '@universis/common/routing';
import { MostModule } from '@themost/angular';
import { StudentAdditionalInfoRoutingModule } from './student-additional-info.routing'
import { StudentAdditionalInfoTableComponent } from './components/student-additional-info-table/student-additional-info-table.component';

@NgModule ({
  imports: [
    CommonModule,
    AdvancedFormsModule,
    SharedModule,
    FormsModule,
    MostModule,
    TablesModule,
    TranslateModule,
    RouterModalModule,
    StudentAdditionalInfoRoutingModule
  ],
  declarations: [
    StudentAdditionalInfoTableComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class StudentAdditionalInfoModule implements OnInit {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading student additional info module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/student-additional-info.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
