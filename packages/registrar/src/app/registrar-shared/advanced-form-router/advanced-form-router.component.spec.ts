import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedFormRouterComponent } from './advanced-form-router.component';

describe('AdvancedFormRouterComponent', () => {
  let component: AdvancedFormRouterComponent;
  let fixture: ComponentFixture<AdvancedFormRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedFormRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedFormRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
