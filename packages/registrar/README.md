## This repo is under development

# @universis/registrar
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/registrar** application is the core application of Universis project for managing students, courses, departments, study programs etc.  
