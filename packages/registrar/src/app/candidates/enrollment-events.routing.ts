import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  CurrentAcademicYearResolver, CurrentAcademicPeriodResolver, ActiveDepartmentIDResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {EnrollmentEventEditComponent} from './components/enrollment-event-edit/enrollment-event-edit.component';
import {EnrollmentEventsHomeComponent} from './components/events-home/events-home.component';
import {
  EnrollmentEventsTableConfigurationResolver,
  EnrollmentEventsTableSearchResolver
} from './components/events-table/events-table-config.resolver';
import {EnrollmentEventsDashboardComponent} from './components/event-dashboard/enrollment-events-dashboard.component';
import {EnrollmentEventRootComponent} from './components/events-root/event-root.component';
import {EnrollmentEventOverviewComponent} from './components/event-dashboard/event-overview/event-overview.component';
import {EnrollmentEventsTableComponent} from './components/events-table/events-table.component';

const routes: Routes = [
  {
    path: '',
    component: EnrollmentEventsHomeComponent,
    data: {
      title: 'EnrollmentEvents'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/opened'
      },
      {
        path: 'list/:list',
        component: EnrollmentEventsTableComponent,
        data: {
          title: 'Enrollment events list'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: EnrollmentEventsTableConfigurationResolver,
          searchConfiguration: EnrollmentEventsTableSearchResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: EnrollmentEventEditComponent,
            outlet: 'modal',
            data: {
              action: 'new',
              model: 'StudyProgramEnrollmentEvents'
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: EnrollmentEventEditComponent,
            outlet: 'modal',
            data: {
              action: 'edit',
              model: 'StudyProgramEnrollmentEvents'
            }
          }
        ]
      }
    ]
  },
  {
    path: 'create',
    component: EnrollmentEventsHomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        resolve: {
          organizer: ActiveDepartmentIDResolver,
          inscriptionYear: CurrentAcademicYearResolver,
          inscriptionPeriod: CurrentAcademicPeriodResolver
        }
      }
    ]
  },
  {
    path: ':id',
    component: EnrollmentEventRootComponent,
    data: {
      title: 'Candidates Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: EnrollmentEventsDashboardComponent,
        data: {
          title: 'Candidates.Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: EnrollmentEventOverviewComponent,
            data: {
              title: 'Candidates.Overview'
            }
          }

        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class EnrollmentEventsRoutingModule {
}
