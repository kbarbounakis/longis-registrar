import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, share } from 'rxjs/operators';

@Injectable()
export class InstructorTableConfigurationResolver implements Resolve<TableConfiguration> {

    constructor(private http: HttpClient) {}

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
            if (route.params.list == null) {
                return this.http.get<TableConfiguration>(`assets/tables/Instructors/instructors-table.config.list.json`);
            }
            return this.http.get(`assets/tables/Instructors/instructors-table.config.${route.params.list}.json`).pipe(
                map((result) => {
                    return result as TableConfiguration;
                }),
                catchError((err) => {
                    return this.http.get<TableConfiguration>(`assets/tables/Instructors/instructors-table.config.list.json`);
                })
            );
    }
}

@Injectable()
export class InstructorTableSearchResolver implements Resolve<any> {
    constructor(private http: HttpClient) {}

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
                if (route.params.list == null) {
                    return this.http.get<TableConfiguration>(`assets/tables/Instructors/instructors-table.search.list.json`);
                }
                return this.http.get(`assets/tables/Instructors/instructors-table.search.${route.params.list}.json`).pipe(
                    map((result) => {
                        return result as TableConfiguration;
                    }),
                    catchError((err) => {
                        return this.http.get<TableConfiguration>(`assets/tables/Instructors/instructors-table.search.list.json`);
                    })
                )
    }
}

@Injectable()
export class InstructorDefaultTableConfigurationResolver implements Resolve<TableConfiguration> {
    
    public source$: Observable<TableConfiguration>;

    constructor(private http: HttpClient) {
        this.source$ = this.http.get<TableConfiguration>(`assets/tables/Instructors/instructors-table.config.list.json`).pipe(
            share()
        );
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> {
        return this.source$;
    }
}
