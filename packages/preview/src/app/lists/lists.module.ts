import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {GroupListsComponent} from './components/group-lists/group-lists.component';
import {ListsRoutingModule} from './lists.routing';
import {SimpleListComponent} from './components/simple-list/simple-list.component';

@NgModule({
  imports: [
    HighlightIncludeModule,
    ListsRoutingModule
  ],
  declarations: [
    GroupListsComponent,
    SimpleListComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ListsModule {}
