import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesOverviewStatsAutoregisteredComponent } from './classes-overview-stats-autoregistered.component';

describe('ClassesOverviewStatsAutoregisteredComponent', () => {
  let component: ClassesOverviewStatsAutoregisteredComponent;
  let fixture: ComponentFixture<ClassesOverviewStatsAutoregisteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesOverviewStatsAutoregisteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesOverviewStatsAutoregisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
