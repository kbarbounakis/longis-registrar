import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-candidate-upload-action-results',
  templateUrl: './candidate-upload-action-results.component.html',
  styleUrls: ['./candidate-upload-action-results.component.scss'],
})
export class CandidateUploadActionResultsComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  private paramsSubscription: Subscription;
  public actionResults: any[];
  public lastMessage: string;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _loadingService: LoadingService
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    this.modalTitle = this._translateService.instant(
      'Candidates.UploadActions.ResultsModal'
    );
    this.modalClass = 'modal-xl';
  }

  ngOnInit() {
    // hide cancel button
    this.cancelButtonClass = 'd-none';
    this.paramsSubscription = this._activatedRoute.params.subscribe(
      async (params) => {
        try {
          this._loadingService.showLoading();
          // clear last message
          this.lastMessage = null;
          // fetch action results (imported candidates)
          this.actionResults = 
            await this._context
              .model('CandidateStudentUploadActionResults')
              .where('result')
              .equal(false)
              .and('action')
              .equal(params.action)
              .getItems();
        } catch (err) {
          console.error(err);
          this.lastMessage = this._translateService.instant(
            'Candidates.UploadActions.FailedToFetchResults'
          );
        } finally {
          // hide loading
          this._loadingService.hideLoading();
        }
      }
    );
  }

  async ok() {
    return this.close();
  }

  async cancel() {
    return this.close();
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
}
