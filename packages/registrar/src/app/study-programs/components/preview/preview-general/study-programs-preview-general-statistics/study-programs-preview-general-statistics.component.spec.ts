import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewGeneralStatisticsComponent } from './study-programs-preview-general-statistics.component';

describe('StudyProgramsPreviewGeneralStatisticsComponent', () => {
  let component: StudyProgramsPreviewGeneralStatisticsComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewGeneralStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewGeneralStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewGeneralStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
