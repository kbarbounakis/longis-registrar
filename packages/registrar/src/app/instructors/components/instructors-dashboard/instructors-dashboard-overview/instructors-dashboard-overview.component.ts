import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-instructors-dashboard-overview',
  templateUrl: './instructors-dashboard-overview.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardOverviewComponent implements OnInit {
  public model: any;


  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department($expand=organization),user')
      .getItem();
  }
}
