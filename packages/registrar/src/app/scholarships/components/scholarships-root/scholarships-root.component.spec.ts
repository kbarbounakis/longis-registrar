import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsRootComponent } from './scholarships-root.component';

describe('ScholarshipsRootComponent', () => {
  let component: ScholarshipsRootComponent;
  let fixture: ComponentFixture<ScholarshipsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
