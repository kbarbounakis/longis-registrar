import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardExamDocumentsComponent } from './dashboard-exam-documents.component';

describe('DashboardExamDocumentsComponent', () => {
  let component: DashboardExamDocumentsComponent;
  let fixture: ComponentFixture<DashboardExamDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardExamDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardExamDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
