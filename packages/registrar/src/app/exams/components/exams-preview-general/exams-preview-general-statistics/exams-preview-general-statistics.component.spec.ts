import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGeneralStatisticsComponent } from './exams-preview-general-statistics.component';

describe('ExamsPreviewGeneralStatisticsComponent', () => {
  let component: ExamsPreviewGeneralStatisticsComponent;
  let fixture: ComponentFixture<ExamsPreviewGeneralStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGeneralStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGeneralStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
