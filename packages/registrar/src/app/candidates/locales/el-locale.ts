const elLocale = {
    abbr: 'el',
    months: 'Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μαίος_Ιούνιος_Ιούλιος_Αύγουστος_Σπετέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος'.split('_'),
    monthsShort: 'Ιαν_Φεβ_Μαρ_Απρ_Μαι_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_ΔΕκ'.split('_'),
    monthsParseExact: true,
    weekdays: 'Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο'.split('_'),
    weekdaysShort: 'Κυρ_Δευ_Τρι_Τετ_Πεμ_Παρ_Σαβ'.split('_'),
    weekdaysMin: 'ΚυρΔε_Τρ_Τε_Πε_Πα_Σα'.split('_'),
    weekdaysParseExact: true,
    longDateFormat: {
        LT: 'HH:mm',
        LTS: 'HH:mm:ss',
        L: 'DD/MM/YYYY',
        LL: 'D MMMM YYYY',
        LLL: 'D MMMM YYYY HH:mm',
        LLLL: 'dddd, D MMMM YYYY HH:mm'
    },
    calendar: {
        sameDay: '[Σήμερα στις] LT',
        nextDay: '[Αύριο στις] LT',
        nextWeek: 'dddd [στις] LT',
        lastDay: '[Χθες στις] LT',
        lastWeek: '[Την τελευταία] dddd [στις] LT',
        sameElse: 'L'
    },
    relativeTime: {
        future: 'σε %s',
        past: 'πριν %s',
        s: 'λίγα δευτερόλεπτα',
        ss: '%d δευτερόλεπτα',
        m: 'ένα λεπτό',
        mm: '%d λεπτά',
        h: 'μία ώρα',
        hh: '%d ώρες',
        d: 'μία μέρα',
        dd: '%d μέρες',
        M: 'ένα μήνας',
        MM: '%d μήνες',
        y: 'ένας χρόνος',
        yy: '%d χρόνια'
    },
    dayOfMonthOrdinalParse: /\d{1,2}η/,
    ordinal: '%dη',
    week: {
        dow: 1,
        // Monday is the first day of the week.
        doy: 4 // The week that contains Jan 4th is the first week of the year.
    }
};
export {
    elLocale
};
