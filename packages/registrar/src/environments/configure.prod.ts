// place code here for configuring or extending application modules
//
// DO NOT PLACE CODE HERE
// THIS MODULE IS INTENDED TO BE USED IN A PRODUCTION ENVIRONMENT ONLY
//

import { NgModuleRef, Type } from '@angular/core';
export function configure(app: NgModuleRef<any>) {
    //
    // place you code here (only in production environments)
    //
}
