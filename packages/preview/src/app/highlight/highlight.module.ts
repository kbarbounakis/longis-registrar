import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HighlightIncludeComponent} from './highlight-include.component';
import {HighlightComponent} from './highlight.component';

@NgModule({
  imports: [],
  declarations: [
    HighlightComponent,
    HighlightIncludeComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    HighlightIncludeComponent,
    HighlightComponent
  ]
})
export class HighlightIncludeModule {
}
