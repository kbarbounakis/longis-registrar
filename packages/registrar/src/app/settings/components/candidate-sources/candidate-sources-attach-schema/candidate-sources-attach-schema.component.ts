import { HttpClient } from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { LoadingService } from "@universis/common";
import { RouterModalOkCancel } from "@universis/common/routing";
import { Subscription } from "rxjs";

@Component({
  selector: "app-candidate-sources-attach-schema",
  templateUrl: "./candidate-sources-attach-schema.component.html",
  styleUrls: ["./candidate-sources-attach-schema.component.scss"],
})
export class CandidateSourcesAttachSchemaComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public lastError: any;
  public attachments: any[] = [];

  private candidateSource: number | string;
  private paramSubscription: Subscription;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _http: HttpClient
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    this.modalTitle = this._translateService.instant("Settings.AttachSchema");
    this.modalClass = "modal-lg";
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      // set ok button text
      this.okButtonText = this._translateService.instant("Settings.Upload");
      // disable ok button by default
      this.okButtonDisabled = true;
      // fetch candidate source id
      this.candidateSource = params.id;
    });
  }

  async ok() {
    try {
      // show loading
      this._loadingService.showLoading();
      // clear last error
      this.lastError = null;
      // get attachment
      const attachment = this.attachments[0];
      if (attachment == null) {
        return;
      }
      // initialize form data and append file
      const formData: FormData = new FormData();
      formData.append("file", attachment, attachment.name);
      // get headers
      const serviceHeaders = this._context.getService().getHeaders();
      // set postUrl
      const postUrl = this._context
        .getService()
        .resolve(
          `CandidateSources/${this.candidateSource}/attachConfigurationSchema`
        );
      // add configuration schema
      await this._http
        .post(postUrl, formData, {
          headers: serviceHeaders,
        })
        .toPromise();
      // and close with reload fragment
      this.close({ fragment: "reload" });
    } catch (err) {
      // set last error
      this.lastError = err;
    } finally {
      // finally, hide loading
      this._loadingService.hideLoading();
    }
  }

  cancel() {
    // close
    return this.close();
  }

  onFileSelect(event) {
    // remove previous file (can only be one)
    if (this.attachments.length) {
      this.attachments.splice(0, 1);
    }
    // add new file
    this.attachments.push(event.addedFiles[0]);
    // and enable ok button
    this.okButtonDisabled = false;
  }

  onFileRemove(event) {
    if (this.attachments.length) {
      // remove file
      this.attachments.splice(this.attachments.indexOf(event), 1);
      // and disable ok button
      this.okButtonDisabled = true;
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
