import { AdvancedTableConfiguration, TableConfiguration } from "@universis/ngx-tables";

export const DATA_AVAILABILITY_CONFIG: TableConfiguration | any = {
    "model": "LocalDepartments",
    "multipleSelect": true,
    "selectable": true,
    "columns": [
        {
            "name": "id",
            "property": "id",
            "formatter": "ButtonFormatter",
            "className": "text-center",
            "formatOptions": {
              "buttonContent": "<i class=\"fas fa-edit text-indigo\"></i>",
              "buttonClass": "btn btn-default",
              "commands": [
                {
                  "outlets": {
                    "modal": [
                      "${id}",
                      "edit"
                    ]
                  }
                }
              ]
            }
          },
        {
            "name": "dataAvailability",
            "title": "Forms.ID",
            "hidden": true
        },
        {
            "name": "alternativeCode",
            "title": "Departments.ListTable.ID",
            "hidden": true
        },
        {
            "name": "name",
            "title": "Students.Department",
            "formatter": "NgClassFormatter",
            "formatOptions": {
                "ngClass": {
                    "py-2 d-block": true
                }
            }
        },
        {
            "name": "availableSince",
            "title": "DataAvailabilityAttributes.DataAvailabilitySince",
            "sortable": false,
            "defaultContent": "-",
            "formatters": [
                {
                    "formatter": "DateTimeFormatter",
                    "formatString": "mediumDate",
                }
            ]
           

        },
        {
            "name": "availableSinceDescription",
            "title": "DataAvailabilityAttributes.DataAvailabilitySinceDescription",
            "formatter": "TemplateFormatter",
            "formatString": "${ availableSinceDescription && availableSinceDescription.length > 80 ? availableSinceDescription.substring(0,80) + '...' : availableSinceDescription }"
        },
        {
            "name": "dateCreated",
            "title": "Settings.Attributes.dateCreated",
            "sortable": false,
            "formatter": "DateTimeFormatter",
            "formatString": "short"
        },
        {
            "name": "dateModified",
            "title": "Settings.Attributes.dateModified",
            "sortable": false,
            "formatter": "DateTimeFormatter",
            "formatString": "short"
        },
        {
            "name": "modifiedBy",
            "title": "Settings.Attributes.modifiedBy",
            "formatter": "TemplateFormatter",
            "formatString": "${modifiedBy != null ? modifiedBy.replace(/@(.*?)$/, '') : ''}"
        },
        {
            "name": "createdBy",
            "title": "Settings.Attributes.createdBy",
            "hidden": true,
            "formatter": "TemplateFormatter",
            "formatString": "${modifiedBy != null ? modifiedBy.replace(/@(.*?)$/, '') : ''}"
        }
    ],
    "criteria": [
    ],
    "defaults": {
        "expand": "dataAvailability"
    },
    "searches": [
    ]
}