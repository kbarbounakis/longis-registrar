import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '@universis/ngx-tables';

@Component({
    selector: 'app-departments-advanced-table-search',
    templateUrl: './departments-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DepartmentsAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
