import { Injectable } from '@angular/core';
import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, share } from 'rxjs/operators';
@Injectable()
export class StudentTableConfigurationResolver implements Resolve<TableConfiguration> {
    constructor(private http: HttpClient) {}

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> {
            return this.get(route.params.list);
    }

    get(list?: string): Observable<TableConfiguration> {
        if (list == null) {
            return this.http.get<TableConfiguration>(`assets/tables/Students/students-table.config.list.json`);
        }
        return this.http.get(`assets/tables/Students/students-table.config.${list}.json`).pipe(
            map((result) => {
                return result as TableConfiguration;
            }),
            catchError((err) => {
                return this.http.get<TableConfiguration>(`assets/tables/Students/students-table.config.list.json`);
            })
        );
    }

}

@Injectable()
export class StudentTableSearchResolver implements Resolve<any> {

    constructor(private http: HttpClient) { }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> {
        if (route.params.list == null) {
            return this.http.get<TableConfiguration>(`assets/tables/Students/students-table.search.list.json`);
        }
        return this.http.get(`assets/tables/Students/students-table.search.${route.params.list}.json`).pipe(
            map((result) => {
                return result as TableConfiguration;
            }),
            catchError((err) => {
                return this.http.get<TableConfiguration>(`assets/tables/Students/students-table.search.list.json`);
            })
        )
    }

}

@Injectable()
export class StudentDefaultTableConfigurationResolver implements Resolve<TableConfiguration> {

    public source$: Observable<TableConfiguration>;

    constructor(private http: HttpClient) {
        this.source$ = this.http.get<TableConfiguration>(`assets/tables/Students/students-table.config.list.json`).pipe(
            share()
        );
    }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<TableConfiguration> {
        return this.source$;
    }

    get(): Observable<TableConfiguration> {
        return this.source$;
    }
}

@Injectable()
export class StudentActiveTableConfigurationResolver implements Resolve<TableConfiguration> {

    public source$: Observable<TableConfiguration>;

    constructor(private http: HttpClient) {
        this.source$ = this.http.get<TableConfiguration>(`assets/tables/Students/students-table.config.active.json`).pipe(
            share()
        );
    }

    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<TableConfiguration> {
        return this.source$;
    }

    get(): Observable<TableConfiguration> {
        return this.source$;
    }
}
