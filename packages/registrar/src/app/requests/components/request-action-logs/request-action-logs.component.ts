import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {AppEventService} from '@universis/common';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-request-action-logs',
  templateUrl: './request-action-logs.component.html'
})
export class RequestActionLogsComponent implements OnInit, OnDestroy {
  @Input('request') request;
  public logs: any;
  private reloadSubscription: Subscription;
  private fragmentSubscription: Subscription;

  constructor(private _context: AngularDataContext,
              private _route: ActivatedRoute,
              private _router: Router,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    // get request logs
    this.getActionEventLogs();
    this.reloadSubscription = this._appEvent.changed.subscribe(async event => {
      if (event && event.target && ((event.target.id === this.request.id)
        || (event.model === 'DocumentNumberSeriesItems'))) {
        // reload logs
       await this.getActionEventLogs();
      }
    });
    this.fragmentSubscription = this._route.fragment.subscribe(async fragment => {
      if (fragment && fragment === 'reload') {
       await this.getActionEventLogs();
      }
    });
  }
  async getActionEventLogs() {
    this.logs = await this._context.model('ActionEventLogs')
      .where('action').equal(this.request.id)
      .take(-1)
      .orderBy('dateCreated')
      .getItems();
  }

  ngOnDestroy(): void {
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

}
