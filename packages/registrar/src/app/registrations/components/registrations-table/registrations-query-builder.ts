import * as studentBuilder from '../../../students/components/students-table/students-query-builder'

const properties: {
    name: string,
    label: string,
    type?: string;
    source?: string,
    template?: string
}[] = [{
    name: 'registrationPeriod',
    label: 'Registrations.AcademicPeriod',
    source: '/AcademicPeriods?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, { 
    name: 'registrationYear',
    label: 'Registrations.AcademicYear',
    source: '/AcademicYears?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'status',
    label: 'Registrations.Status',
    source: '/PeriodRegistrationStatuses?$top=-1&$select=id as raw,name as label&$orderby=id desc'
}, {
    name: 'registrationDate',
    label: 'Registrations.ListTable.RegistrationDate'
}, 
{
    name: 'semester',
    label: 'Registrations.Semester'
}];


function addSharedProperties(): {
    name: string;
    label: string;
    type?: string;
    source?: string;
    template?: string;
}[] {
    const studentProperties = studentBuilder.sharedProperties;
    studentProperties.forEach(el => el.name = `student/${el.name}`)

    return studentProperties;
}

properties.push(...addSharedProperties());

export { properties };

