import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class ScholarshipsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./scholarships-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./scholarships-table.config.list.json`);
        });
    }
}

export class ScholarshipsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./scholarships-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./scholarships-table.search.list.json`);
            });
    }
}

export class ScholarshipsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./scholarships-table.config.list.json`);
    }
}
