import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidatesSharedModule } from './candidates.shared';
import { TranslateModule } from '@ngx-translate/core';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { RouterModalModule } from '@universis/common/routing';
import { EnrollmentEventsRoutingModule } from './enrollment-events.routing';
import {RouterModule} from '@angular/router';
import {EnrollmentEventEditComponent} from './components/enrollment-event-edit/enrollment-event-edit.component';
import {StudyProgramsSharedModule} from '../study-programs/study-programs.shared';
import {StudentsSharedModule} from '../students/students.shared';
import {BsDatepickerModule, TabsModule, TimepickerModule} from 'ngx-bootstrap';
import {ElementsModule} from '../elements/elements.module';
import {ChartsModule} from 'ng2-charts';
import {MessagesSharedModule} from '../messages/messages.shared';
import {SendMessageToStudentComponent} from '../messages/components/send-message-to-student/send-message-to-student.component';
import {EnrollmentEventsDashboardComponent} from './components/event-dashboard/enrollment-events-dashboard.component';
import {EnrollmentEventsTableComponent} from './components/events-table/events-table.component';
import {EnrollmentEventRootComponent} from './components/events-root/event-root.component';
import {EnrollmentEventOverviewComponent} from './components/event-dashboard/event-overview/event-overview.component';
import {EnrollmentEventOverviewDocumentsComponent} from './components/event-dashboard/event-overview/events-overview-documents/event-overview-documents.component';
import {EnrollmentEventsHomeComponent} from './components/events-home/events-home.component';
import {EnrollmentEventRequestsComponent} from './components/event-dashboard/event-overview/event-overview-requests/event-overview-requests.component';
import { AdminSharedModule } from '@universis/ngx-admin';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    CandidatesSharedModule,
    EnrollmentEventsRoutingModule,
    TablesModule,
    SharedModule,
    FormsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    StudyProgramsSharedModule,
    StudentsSharedModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    ElementsModule,
    ChartsModule,
    MessagesSharedModule,
    AdminSharedModule
  ],
  declarations: [
    EnrollmentEventsHomeComponent,
    EnrollmentEventRootComponent,
    EnrollmentEventsTableComponent,
    EnrollmentEventEditComponent,
    EnrollmentEventsDashboardComponent,
    EnrollmentEventOverviewComponent,
    EnrollmentEventOverviewDocumentsComponent,
    EnrollmentEventRequestsComponent
  ],
  entryComponents: [
    SendMessageToStudentComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EnrollmentEventsModule { }
