import { Injectable, Injector } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ServerEvent, ServerEventSubscriber } from '@universis/common';

@Injectable()
export class MessageSubscriber implements ServerEventSubscriber {
    constructor(private context: AngularDataContext) {
        //
    }
    subscribe(event: ServerEvent): void {
        if (event == null) {
            return;
        }
        if (event.entityType === 'Message') {
            // todo::handle student messages
        }
    }
}
